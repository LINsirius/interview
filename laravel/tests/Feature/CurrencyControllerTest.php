<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Support\Arr;
use App\Http\CurrencyExchangeData;

class CurrencyControllerTest extends TestCase
{
    use WithoutMiddleware;  // 停用中介層

    /**
     * A basic test example.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->mockClass();
    }

    private function mockClass()
    {
        $this->fakeCurrencyExchangeService = \Mockery::mock(CurrencyExchangeService::class);
        $this->app->instance(CurrencyExchangeService::class, $this->fakeCurrencyExchangeService);
    }

    /**
     * @test
     */
    public function source或target錯誤時要回應錯誤()
    {
        $input = [
            'source' => 'XXX',
            'target' => 'XXX',
            'amount' => '100',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(422);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(!empty($res));
        $this->assertTrue(Arr::has($res, 'retVal.source'));
        $this->assertTrue(Arr::has($res, 'retVal.target'));

        $input = [
            'source' => 'TWD',
            'target' => 'XXX',
            'amount' => '1,000',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(422);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(!empty($res));
        $this->assertTrue(!Arr::has($res, 'retVal.source'));
        $this->assertTrue(Arr::has($res, 'retVal.target'));

        $input = [
            'source' => 'XXX',
            'target' => 'TWD',
            'amount' => '1,000',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(422);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(!empty($res));
        $this->assertTrue(Arr::has($res, 'retVal.source'));
        $this->assertTrue(!Arr::has($res, 'retVal.target'));
    }

    /**
     * @test
     */
    public function 回應成功()
    {
        $data = (new CurrencyExchangeData)->data;

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => '1,000',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(200);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(!empty($res));
        $this->assertEquals(1, Arr::get($res, 'retCode'));
        $expect = round(1000 * $data['currencies'][$input['source']][$input['target']], 2);
        $expect = _number_format($expect);
        $this->assertEquals($expect, Arr::get($res, 'retVal.amount'));

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => '1,000,000',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(200);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(!empty($res));
        $this->assertEquals(1, Arr::get($res, 'retCode'));
        $expect = round(1000000 * $data['currencies'][$input['source']][$input['target']], 2);
        $expect = _number_format($expect);
        $this->assertEquals($expect, Arr::get($res, 'retVal.amount'));

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => '1,000,000.1',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(200);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(!empty($res));
        $this->assertEquals(1, Arr::get($res, 'retCode'));
        $expect = round(1000000.1 * $data['currencies'][$input['source']][$input['target']], 2);
        $expect = _number_format($expect);
        $this->assertEquals($expect, Arr::get($res, 'retVal.amount'));

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => '999,999.999',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(200);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(!empty($res));
        $this->assertEquals(1, Arr::get($res, 'retCode'));
        $expect = round(999999.999 * $data['currencies'][$input['source']][$input['target']], 2);
        $expect = _number_format($expect);
        $this->assertEquals($expect, Arr::get($res, 'retVal.amount'));
    }

    /**
     * @test
     */
    public function amount錯誤時要回應錯誤()
    {
        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => '1,x000',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(422);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(Arr::has($res, 'retVal.amount'));

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => '00000000',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(422);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(Arr::has($res, 'retVal.amount'));

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => '0,123',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(422);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(Arr::has($res, 'retVal.amount'));

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => '1.0.0',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(422);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(Arr::has($res, 'retVal.amount'));

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => '100000,,,,,0',
        ];

        $actual = $this->call('GET', '/api/currency_exchang', $input);
        $actual->assertStatus(422);
        $res = $actual->content();
        $res = json_decode($res, true);
        $this->assertTrue(Arr::has($res, 'retVal.amount'));
    }
}
