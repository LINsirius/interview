<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Support\Arr;
use App\Http\CurrencyExchangeData;
use App\Http\Services\CurrencyExchangeService;

class CurrencyExchangeServiceTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->mockClass();
    }

    private function mockClass()
    {
    }

    /**
     * @test
     */
    public function source或target錯誤時要回應錯誤()
    {
        $obj = new CurrencyExchangeService(new CurrencyExchangeData);

        $input = [
            'source' => 'XXX',
            'target' => 'XXX',
            'amount' => 100,
        ];

        $res = $obj->handle($input['source'], $input['target'], 100);
        $this->assertFalse($res);

        $input = [
            'source' => 'TWD',
            'target' => 'XXX',
            'amount' => 1000,
        ];

        $res = $obj->handle($input['source'], $input['target'], 1000);
        $this->assertFalse($res);

        $input = [
            'source' => 'XXX',
            'target' => 'TWD',
            'amount' => 1000,
        ];

        $res = $obj->handle($input['source'], $input['target'], 1000);
        $this->assertFalse($res);
    }

    /**
     * @test
     */
    public function 回應成功()
    {
        $CurrencyExchangeData = (new CurrencyExchangeData);
        $data = $CurrencyExchangeData->data;
        $obj = new CurrencyExchangeService($CurrencyExchangeData);
        
        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => 1000,
        ];

        $res = $obj->handle($input['source'], $input['target'], $input['amount']);
        $expect = round($input['amount'] * $data['currencies'][$input['source']][$input['target']], 2);
        $this->assertEquals($expect, $res);

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => 1000000,
        ];

        $res = $obj->handle($input['source'], $input['target'], $input['amount']);
        $expect = round($input['amount'] * $data['currencies'][$input['source']][$input['target']], 2);
        $this->assertEquals($expect, $res);

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => 1000000.1,
        ];

        $res = $obj->handle($input['source'], $input['target'], $input['amount']);
        $expect = round($input['amount'] * $data['currencies'][$input['source']][$input['target']], 2);
        $this->assertEquals($expect, $res);

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => 999999.999,
        ];

        $res = $obj->handle($input['source'], $input['target'], $input['amount']);
        $expect = round($input['amount'] * $data['currencies'][$input['source']][$input['target']], 2);
        $this->assertEquals($expect, $res);
    }

    /**
     * @test
     */
    public function amount錯誤時要回應錯誤()
    {
        $obj = new CurrencyExchangeService(new CurrencyExchangeData);

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => '1,x000',
        ];

        $res = $obj->handle($input['source'], $input['target'], $input['amount']);
        $this->assertFalse($res);

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => [],
        ];

        $res = $obj->handle($input['source'], $input['target'], $input['amount']);
        $this->assertFalse($res);

        $input = [
            'source' => 'TWD',
            'target' => 'JPY',
            'amount' => null,
        ];

        $res = $obj->handle($input['source'], $input['target'], $input['amount']);
        $this->assertFalse($res);
    }
}
