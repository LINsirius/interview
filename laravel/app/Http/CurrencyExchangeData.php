<?php

namespace App\Http;

use App\Interfaces\CurrencyExchangeDataInterface;

class CurrencyExchangeData implements CurrencyExchangeDataInterface
{
    public $data = [
        "currencies" => [
            "TWD" => [
                "TWD" => 1,
                "JPY" => 3.669,
                "USD" => 0.03281
            ],
            "JPY" => [
                "TWD" => 0.26956,
                "JPY" => 1,
                "USD" => 0.00885
            ],
            "USD" => [
                "TWD" => 30.444,
                "JPY" => 111.801,
                "USD" => 1
            ],
        ],
    ];

    public function set(array $data) :void
    {
        $this->data = $data;
    }

    public function get() :array
    {
        return is_array($this->data) ? $this->data : [];
    }
}
