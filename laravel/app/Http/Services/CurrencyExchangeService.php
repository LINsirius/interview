<?php

namespace App\Http\Services;

use App\Interfaces\CurrencyExchangeDataInterface;

class CurrencyExchangeService
{
    public function __construct(CurrencyExchangeDataInterface $CurrencyExchangeData)
    {
        $this->data = $CurrencyExchangeData->get();
    }

    public function handle(string $source='', string $target='', $amount=0)
    {
        if (!isset($this->data['currencies'])) {
            return false;
        }

        $currencies = $this->data['currencies'];

        if (!isset($currencies[$source]) || !isset($currencies[$source][$target])) {
            return false;
        }

        if (!is_numeric($amount)) {
            return false;
        }

        return round($amount * $currencies[$source][$target], 2);
    }
}
