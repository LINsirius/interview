<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Currency\ExchangeRequest;
use App\Http\Services\CurrencyExchangeService;
use App\Http\CurrencyExchangeData;

class CurrencyController extends Controller
{
    public function __construct()
    {
    }

    public function exchange(ExchangeRequest $request)
    {
        $service = new CurrencyExchangeService(new CurrencyExchangeData);

        $amount = (float) str_replace(',', '', $request->amount);

        $res = $service->handle($request->source, $request->target, $amount);
        if (!is_numeric($res)) {
            return $this->failOutput('', 'unknown currency.', [], 200);
        }

        $result = '';
        $arr = explode('.', $res);
        $result .= number_format($arr[0]);
        if (isset($arr[1])) {
            $result .= ".{$arr[1]}";
        }

        return $this->successOutput('success', ['amount'=>$result]);
    }
}
