<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * 處理api回應成功的格式.
     *
     * @param string $retMsg
     * @param string $retVal
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function successOutput($retMsg = '', $retVal = [])
    {
        return response()->json([
            'retCode' => 1, // api回應的結果
            'retMsg' => $retMsg, // api回應的代碼訊息
            'retVal' => $retVal, // api回應的值
        ], 200, [], 320); // JSON_UNESCAPED_SLASHES+JSON_UNESCAPED_UNICODE
    }

    /**
     * 處理api回應失敗的格式.
     *
     * @param string $errCode
     * @param string $retMsg
     * @param object $retVal
     * @param int $status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function failOutput($errCode = '', $retMsg = '', $retVal = [], $status = 500)
    {
        return response()->json([
            'retCode' => 0, // api回應的結果
            'errCode' => $errCode, // api回應的錯誤代碼
            'retMsg' => $retMsg, // api回應的代碼訊息
            'retVal' => $retVal, // api回應的值
        ], $status, [], 320); // JSON_UNESCAPED_SLASHES+JSON_UNESCAPED_UNICODE
    }
}
