<?php

if (!function_exists('_number_format')) {
    function _number_format($number=0)
    {
        if (!is_numeric($number)) {
            return false;
        }

        $result = '';
        $arr = explode('.', (string)$number);
        $result .= number_format($arr[0]);
        if (isset($arr[1])) {
            $result .= ".{$arr[1]}";
        }

        return $result;
    }
}
