<?php

namespace App\Interfaces;

interface CurrencyExchangeDataInterface
{
    public function set(array $data) :void;

    public function get() :array;
}