<?php

namespace App\Rules;

use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Validation\Rule;

class NumberRule implements Rule
{
    public $attribute = '';

    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;

        $regexs = implode('|', [
            '[1-9]{1}\d{0,2}(,\d{3})*(\.\d*[1-9]){0,1}',
            '[1-9]{1}\d*(\.\d*[1-9]){0,1}',
            '0(\.\d*[1-9]){0,1}'
        ]);

        if (!preg_match("/^({$regexs})$/", $value)) {
            return false;
        }

        return true;
    }

    public function message()
    {
        return "{$this->attribute} is invalid number.";
    }
}
