<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, \Throwable $exception)
    {
        // input欄位驗證失敗
        if ($exception instanceof ValidationException) {
            return response()->json([
                'retCode' => 0,
                'errCode' => '',
                'retMsg' => $exception->getMessage(),
                'retVal' => $exception->errors(),
            ], 422, [], 320);
        }

        return response()->json([
            'retCode' => 0,
            'errCode' => '',
            'retMsg' => $exception->getMessage(),
            'retVal' => [],
        ], 200, [], 320);
    }
}
