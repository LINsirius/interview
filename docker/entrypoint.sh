#!/bin/bash -eu

nginx -v
php -v

composer install

: ${WWW_PATH:="/var/www"}
uid=$(stat -c %u $WWW_PATH)
gid=$(stat -c %g $WWW_PATH)

[ -z "$(getent group $gid)" ] && groupmod -g $gid ${WWW_GROUP}
[ -z "$(getent passwd $uid)" ] && usermod -u $uid ${WWW_USER}

supervisord -c /etc/supervisor/supervisord.conf --nodaemon
