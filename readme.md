啟動說明
```
cd docker
docker build -t php8-nginx .
cp .env.example .env
docker-compose up -d
```

路由說明
```
[GET] api/currency_exchang 幣值轉換
```

測試路徑
```
laravel/tests/Feature
laravel/tests/Unit
```

配置說明
```
os: debian:stable-slim
php: 8.2
laravel: 10
```